import pandas as pd
import xgboost as xgb
import numpy as np
import time

# from dask import compute, delayed

def get_X_train_y_train(y_target_gene):
  """
  y_target_gene = string of name
  """
  # select only the target gene row
  y_pandas = all_exp.loc[all_exp['Gene'] == y_target_gene]
  # drop the name of the target gene
  y_pandas = y_pandas.drop(columns='Gene')
  # convert expression values to np array (and un-nest it w/ flatten)
  y_arr = y_pandas.to_numpy().flatten()

  ### X values ###

  # select all EXCEPT target
  X_pandas = all_exp.loc[all_exp['Gene'] != y_target_gene]
  # drop the name of the target gene
  X_pandas = X_pandas.drop(columns='Gene')
  # convert expression values to np array (and un-nest it w/ flatten)
  X_arr = X_pandas.to_numpy()

  return X_arr, y_arr

def oneXGTrain(target_y_gene):
  startFor = time.time()
  # do xg
  X_arr, y_arr = get_X_train_y_train(target_y_gene)
  params = {'verbose': 3, 'n_jobs': -1, 'nthread': 1, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 8, 'n_estimators': 1000, 'objective': 'reg:squarederror', 'seed': 42}
  xgb_r = xgb.XGBRegressor(**params)
  xgb_r.fit(X_arr.transpose(), y_arr)

  featImportanceDict = xgb_r.get_booster().get_score(importance_type='gain')

  # also strip white space. Convert to int.
  # also... I didn't see any f in there. Only has F on HAL computer.
  featImportanceDict = {int(k[1:].strip()):v for k,v in featImportanceDict.items()} # keys become ints

  stopFor = time.time() - startFor
  print(f"One tree (seconds): {stopFor.__round__(2)}")

  return (target_y_gene, featImportanceDict)

  # for featNum, importance in featImportanceDict.items():
  #   feat_imp_matrix[idx][featNum] = importance

# pandas data fetch. No splitting.
def get_pandas_data(y_target_gene):
  """
  y_target_gene = string of name
  return: X (PANDAS), y_arr (np array)
  """
  y_train = all_exp.loc[all_exp['Gene'] == y_target_gene]
  y_train = y_train.drop(columns='Gene')
  y_train = y_train.to_numpy().flatten()

  X_train = all_exp.loc[all_exp['Gene'] != y_target_gene]
  X_train = X_train.drop(columns='Gene')
  X_train = X_train.transpose()

  return X_train, y_train

'''
MAIN FUNCTION
'''

all_exp = pd.read_csv('all_exp_imputed_nans.csv')
# ordered_feature_names_list = all_exp.columns.values.tolist()
# ordered_feature_names_list.remove('Gene')
ordered_gene_names_list = all_exp.get("Gene").tolist()

"""
*NEW*
PANDAS TRAINING LOOP 
EXPERIMENTAL
"""

feat_imp_matrix = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))

print("STARTING TRAINING LOOP")

fullTrain = time.time()
stop_early = 0
for idx, target_y_gene in enumerate(ordered_gene_names_list[0:25]):
  print(idx, "loop of 6,271")
  X_train, y_train = get_pandas_data(target_y_gene)
  # x.shape = 6270 x 954
  # y.shape = 954

  dtrain = xgb.DMatrix(X_train, label=y_train, missing=np.NaN)

  params = {'verbose': 3, 'n_jobs': -1, 'nthread': 1, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 8, 'n_estimators': 1000, 'objective': 'reg:squarederror', 'seed': 42}

  xgb_r = xgb.XGBRegressor(**params)
  xgb_r.fit(X_train, y_train)

  featImportanceDict = xgb_r.get_booster().get_score(importance_type='gain')
  print("importance_dict (len and full thing):")
  print(len(featImportanceDict.items()))
  print(featImportanceDict)

  # also strip white space. Convert to int.
  # also... I didn't see any f in there 
  featImportanceDict = {int(k.strip()):v for k,v in featImportanceDict.items()} # keys become ints
  print(featImportanceDict)

  for featNum, importance in featImportanceDict.items():
    feat_imp_matrix[idx][featNum] = importance
  
  if stop_early >= 20:
    break 
  stop_early += 1

np.savetxt("_dec_11__FIRST5_pandas_ft_importance.csv", feat_imp_matrix, delimiter=",")

gpu_time = time.time() - fullTrain
print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
print("Fin")
