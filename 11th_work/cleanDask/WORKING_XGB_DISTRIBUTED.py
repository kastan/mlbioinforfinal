import time
import xgboost as xgb
import numpy as np
import pandas as pd
import dask.array as da
import dask.dataframe as dd
# import dask.distributed
from dask.distributed import Client
from dask_cuda import LocalCUDACluster

'''
GLOBAL: Load data
'''
print("Starting file")
gold_df = pd.read_csv('../gold_data/all_exp_imputed_nans_gold_standard.csv') # KEEP HEADER
ordered_gene_names_list = pd.read_csv('../gold_data/ordered_gene_names_list_gold_standard.csv', header=None) # NO HEADER
ordered_gene_names_list = ordered_gene_names_list.iloc[:, 0].to_list()
# ordered_gene_names_list = ordered_gene_names_list.values.tolist()
# print(f"ordered genes list: {ordered_gene_names_list}")
# print(gold_df.head())
# print(len(ordered_gene_names_list)) # 3165
# end global

# y_train = gold_df.loc[gold_df['Gene'] == 'YAL003W']
# print("ytrain here:")
# print(y_train)
# print("ok")

# pandas data fetch. No splitting.
def get_pandas_data(y_target_gene):
  """
  y_target_gene = string of name
  return: X (PANDAS), y_arr (np array)
  """
  y_train = gold_df.loc[gold_df['Gene'] == y_target_gene]
  y_train = y_train.drop(columns='Gene')
  y_train = y_train.to_numpy().flatten()

  X_train = gold_df.loc[gold_df['Gene'] != y_target_gene]
  X_train = X_train.drop(columns='Gene')
  X_train = X_train.transpose()

  return X_train, y_train

  # Gold standard networks are represented as TSVs, source \t sink

def read_net(filename):
  '''
  Gold standard networks are represented as TSVs, source \t sink
  Function to read a gold standard network file into an adjacency matrix
  Returns adjacency matrix, direction of edge is row->col
  '''
  # read the adjacency list into a dataframe
  adj_list = pd.read_csv(filename, sep="\t", header=0, names=['source', 'sink'])

  # convert the adj list df to adj matrix df
  adj_mat = pd.crosstab(adj_list.source, adj_list.sink)

  # get the indexes to all the gene IDs in source and sink to make matrix square
  idx = adj_mat.columns.union(adj_mat.index)
  adj_mat = adj_mat.reindex(index=idx, columns=idx, fill_value=0)

  return adj_mat

def get_gold_genes_list():
  mi2ko = read_net("../../gold/MacIsaac2.KO.txt")
  mi2nv = read_net("../../gold/MacIsaac2.NatVar.txt")
  mi2stress = read_net("../../gold/MacIsaac2.Stress.txt")
  ytract3ko = read_net("../../gold/YEASTRACT_Count3.KO.txt")
  ytract3nv = read_net("../../gold/YEASTRACT_Count3.NatVar.txt")
  ytract3stress = read_net("../../gold/YEASTRACT_Count3.Stress.txt")
  ytract2ko = read_net("../../gold/YEASTRACT_Type2.KO.txt")
  ytract2nv = read_net("../../gold/YEASTRACT_Type2.KO.txt")
  ytract2stress = read_net("../../gold/YEASTRACT_Type2.KO.txt")

  mi2 = pd.concat([mi2ko, mi2nv, mi2stress], join='outer')
  mi2 = mi2.fillna(0).astype(int)
  mi2 = mi2.loc[~mi2.index.duplicated(),~mi2.columns.duplicated()]

  ytract3 = pd.concat([ytract3ko, ytract3nv, ytract3stress], axis=1)
  ytract3 = ytract3.fillna(0).astype(int)
  ytract3 = ytract3.loc[~ytract3.index.duplicated(),~ytract3.columns.duplicated()]

  ytract2 = pd.concat([ytract2ko, ytract2nv, ytract2stress], axis=1)
  ytract2 = ytract2.fillna(0).astype(int)
  ytract2 = ytract2.loc[~ytract2.index.duplicated(),~ytract2.columns.duplicated()]

  mi2_genes = mi2.columns
  ytract3_genes = ytract3.columns
  ytract2_genes = ytract2.columns
  all_gold_standard_genes = mi2.columns.union(ytract3.columns.union(ytract2.columns))
  return all_gold_standard_genes


def main(client):
  # X and y must be Dask dataframes or arrays
  print(client)
  print("Should be a url^^^")

  feat_imp_matrix = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))

  fullTrain = time.time()
  stop_early = 0
  for idx, target_y_gene in enumerate(ordered_gene_names_list):
    if idx <= 1500:
      continue

    print(idx, "loop of 3.1k only gold")
    oneRound = time.time()


    X_train, y_train = get_pandas_data(target_y_gene)
    # x.shape = 6270 x 954
    # y.shape = 954

    X_train = dd.from_pandas(X_train, npartitions=1)
    y_train = da.from_array(y_train) # chunks = 'auto

    '''
    dtrain = xgb.dask.DaskDMatrix(
      client=client,
      data=taxi_train[features],
      label=taxi_train[y_col]
    )
    '''
    dtrain = xgb.dask.DaskDMatrix(client=client, data=X_train, label=y_train)

    # nthread = number of threads per dask worker.
    # nthread 8 = 1.29 sec per run.
    # ntrhead 1 = 0.98 or 0.93 with 20 boosts, 1.25 with 40 boosts.
    # ntrhead 2 = 1.25
    # todo: delete n_estimators and verbose
    # 'n_estimators': 1000,
    params = {'n_jobs': -1, 'nthread': 1, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 8, 'objective': 'reg:squarederror', 'seed': 42}

    output = xgb.dask.train(
    client,
    params,
    dtrain,
    num_boost_round=40,
    )

    xgb_r = output['booster']

    featImportanceDict = xgb_r.get_score(importance_type='gain')

    # also strip white space. Convert to int.
    # also... I didn't see any f in there
    featImportanceDict = {int(k.strip()):v for k,v in featImportanceDict.items()} # keys become ints

    for featNum, importance in featImportanceDict.items():
        # print(f'idx: {idx}, featNum {featNum}')
        feat_imp_matrix[idx][featNum] = importance

    oneTree = time.time() - oneRound
    print(f"One tree time: {oneTree.__round__(2)}")


    if stop_early != 0 and stop_early % 500 == 0:
        filename = f"progress/{str(idx)}_PROGRESS_dec_11_FULL_ONLYGOLD_ftimp.csv"
        np.savetxt(filename, feat_imp_matrix, delimiter=",")
    stop_early += 1

  np.savetxt("_FULL_dec_11_ONLYGOLD_ftimp.csv", feat_imp_matrix, delimiter=",")

  gpu_time = time.time() - fullTrain
  print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
  print("Fin")



if __name__ == "__main__":
  ## memeory_limit = '64GB'
  with LocalCUDACluster(n_workers=4, memory_limit="32 GiB") as cluster:
    with Client(cluster) as client:
      main(client)

