import pandas as pd 
import numpy as np
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
rs = RandomState(MT19937(SeedSequence(123456789)))


# for j in range(2):
#   print('new set')
#   for i in range(5):
#     print(np.random.uniform(-3000,3000))

# exit()

feats = pd.read_csv("/Users/kastanday/code/mlbioinforfinal/finalFigures/bigger_tree/6270from0to7000_dec_13_ALLGENES_totalgain.csv", header=None)
# nonzeroidx = feats.to_numpy().nonzero()
randFeats = feats.copy()


count = 0 
# for el in nonzeroidx:
#   print(f"first el {el}")
#   print(count)
#   count +=1

for index, series in feats.iterrows():
  # print(series)
  # for val in series:
  #   print(val)
  row = np.array(list(series))
  indexes = row.nonzero()
  print(f"index: {index}")
  print(f"num non-zero {len(indexes[0])}") 
  print(indexes)
  randNums = np.random.uniform(-3000,3000,151)
  for i, idx in enumerate(indexes): 
    print("BEFORE")
    print(f"{randFeats.iloc[index][idx]}")
    randFeats.iloc[index][idx] += randNums[i]
    randFeats.iloc[index][idx] = randFeats.iloc[index][idx]
    print("AFTER")
    print(f"{randFeats.iloc[index][idx]}")

  # randFeats.iloc[index][indexes] += 1
  count += 1
  if count > 6:
    break
exit()
np.savetxt("RandomShiftOfFeats2500.csv", randFeats, delimiter=",")

all_exp = pd.read_csv("all_exp.csv")
all_exp.sampe(5)

# all_exp # (6271, 956)
del all_exp['Unnamed: 0']
print(all_exp.shape)

ordered_gene_names_list = all_exp.get("Gene").tolist()
ftNames = all_exp.columns.tolist()

print(len(ordered_gene_names_list))
print(len(ftNames))

df = pd.DataFrame(np.random.uniform(-15,15,size=(6271, 955)), columns=ftNames)
print(df.sample(5))

print("Shape")
print(df.shape)

# np.savetxt("Random_classifier_range30.csv", df, delimiter=",")

# generate dataframe of random values of size 6271, 955, with columns= ftNames, rows = ordered_gene_names_list
