# '''
# Flush print statements
# '''

# import sys
# import builtins
# import os
# sys.stdout = open("stdout.txt", "w", buffering=1)
# def print(text):
#     builtins.print(text)
#     os.fsync(sys.stdout)

# print("This is immediately written to stdout.txt")

# %%
USE_IMPUTE_NANS = True    # False simply removes all rows containg nans.
KNN_IMPUTE_NEIGHBORS_INT = 5         # Impute only, number of neighbors.

# %%
import pandas as pd

ko_exp = pd.read_csv("KO.txt", delimiter='\t')
nat_var_exp = pd.read_csv("NatVar.txt", delimiter='\t')
stress_exp = pd.read_csv("Stress.txt", delimiter='\t')

stress_exp = stress_exp.rename(columns={'Name':'Gene'}) # standardize names

# join 3 expression datasets
merged = pd.merge(
    ko_exp,
    nat_var_exp,
    how='outer',
    on='Gene',
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
)

all_exp = pd.merge(
    merged,
    stress_exp,
    how='outer',
    on='Gene',
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
)

ordered_feature_names_list = all_exp.columns.values.tolist()
ordered_feature_names_list.remove('Gene')
ordered_gene_names_list = all_exp.get("Gene").tolist()


# %%
import xgboost as xgb
import numpy as np
from sklearn.model_selection import train_test_split

def get_X_train_y_train(y_target_gene):
  """
  y_target_gene = string of name
  """
  # select only the target gene row
  y_pandas = all_exp.loc[all_exp['Gene'] == y_target_gene]
  # drop the name of the target gene
  y_pandas = y_pandas.drop(columns='Gene')
  # convert expression values to np array (and un-nest it w/ flatten)
  y_arr = y_pandas.to_numpy().flatten()

  ### X values ###

  # select all EXCEPT target
  X_pandas = all_exp.loc[all_exp['Gene'] != y_target_gene]
  # drop the name of the target gene
  X_pandas = X_pandas.drop(columns='Gene')
  # convert expression values to np array (and un-nest it w/ flatten)
  X_arr = X_pandas.to_numpy()


  return X_arr, y_arr

# %%
from sklearn.impute import KNNImputer
from sklearn.impute import SimpleImputer

imputer = None

def impute_nans(all_exp):
    """
    Imputes nans for WHOLE dataset.
    Fit -> transform -> convert back to pandas
    """

    print("Total nans BEFORE imputation, per feature:\n", all_exp.loc[:, all_exp.columns != 'Gene'].isnull().sum())

    imputer = KNNImputer(n_neighbors=KNN_IMPUTE_NEIGHBORS_INT, weights='uniform', metric='nan_euclidean')
    imputer.fit(all_exp.loc[:, all_exp.columns != 'Gene'])

    all_exp = imputer.transform(all_exp.loc[:, all_exp.columns != 'Gene'])
    print("Total nan's AFTER imputation: ", sum(np.isnan(all_exp).flatten()))

    # convert from ndarray back to Pandas

    # dones't include the 'Gene' column
    all_exp = pd.DataFrame(all_exp, columns = ordered_feature_names_list)
    # add Gene column
    all_exp.insert(0, 'Gene', ordered_gene_names_list)
    print(f"Data shape: {all_exp.shape}") # expect (6271, 955)

    all_exp.to_csv("all_exp_imputed_nans.csv")

    return all_exp


# IMPUTE
if (USE_IMPUTE_NANS):
  all_exp = impute_nans(all_exp)
else:
  # drop all rows with nans
  all_exp = all_exp.dropna()
  print(f"Nan rows DROPPED. Total null values in data: {all_exp.isnull().sum()}")

print(f"Nan rows DROPPED. Total null values in data: {all_exp.isnull().sum()}")


# %%
def get_feature_importance_dict(xgb_r):
  importance_dict = xgb_r.get_booster().get_score(importance_type='gain')
  # print(importance_dict)

  importance_list = list(xgb_r.get_booster().get_score().values())
  # print(len(importance_list))

  return importance_dict

# %%
# MAIN TRAINING LOOP

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE


# dim: genes x genes
feat_imp_matrix = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))

dict_of_forests = dict() # geneName: xgb_r
dict_of_rmse = dict() # geneName: rmse

print("STARTING TRAINING LOOP")
tmp = time.time()
# gene_name
for idx, target_y_gene in enumerate(ordered_gene_names_list):
  print(idx, "loop of 6,271")
  # X_train, X_test, y_train, y_test = get_X_train_y_train(target_y_gene)
  X_arr, y_arr = get_X_train_y_train(target_y_gene)
  # x.shape = 6270 x 954
  # y.shape = 954

  # kaggle_params = {
  #             'objective':['reg:squarederror'],
  #             'learning_rate': [0.05, 0.08, 0.1], #so called `eta` value
  #             'max_depth': [2,3, 4],
  #             'min_child_weight': [6, 7, 8],
  #           #   'silent': [1],
  #           #   'subsample': [0.7],
  #           #   'colsample_bytree': [0.7],
  #             'n_estimators': [1000, 2000]
  # }
  # xgb_grid = GridSearchCV(xgb_r,
  #                       kaggle_params,
  #                       cv = 4,
  #                       n_jobs = -1, # -1 uses all cores
  #                       scoring = 'explained_variance',
  #                       verbose=3)

  # best prams from search 
  params = {'verbose': 3, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 8, 'n_estimators': 1000, 'objective': 'reg:squarederror', 'seed': 42}

  xgb_r = xgb.XGBRegressor(**params)
  xgb_r.fit(X_arr.transpose(), y_arr)


  dict_of_forests[target_y_gene] = xgb_r
  featImportanceDict = xgb_r.get_booster().get_score(importance_type='gain')

  # also strip white space. Convert to int.
  # also... I didn't see any f in there. Only has F on HAL computer.
  featImportanceDict = {int(k[1:].strip()):v for k,v in featImportanceDict.items()} # keys become ints

  for featNum, importance in featImportanceDict.items():
    feat_imp_matrix[idx][featNum] = importance
  
  if idx % 500 == 0:
    # save a temporary file
    filename = str(idx) + "_GPU_SAVE_DEC_10.csv"
    np.savetxt(filename, feat_imp_matrix, delimiter=",")


np.savetxt("__DEC_10_feat_imp.csv", feat_imp_matrix, delimiter=",")

# save to csv
# np.savetxt("GPU_FEAT_IMPORTANCE_MATRIX.csv", feat_imp_matrix, delimiter=",")


gpu_time = time.time() - tmp
print("GPU Training Time: %s seconds" % (str(gpu_time)))
