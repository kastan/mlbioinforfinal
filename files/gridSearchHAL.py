
import pandas as pd

ko_exp = pd.read_csv("KO.txt", delimiter='\t')
nat_var_exp = pd.read_csv("NatVar.txt", delimiter='\t')
stress_exp = pd.read_csv("Stress.txt", delimiter='\t')

stress_exp = stress_exp.rename(columns={'Name':'Gene'}) # standardize names

# join 3 expression datasets
merged = pd.merge(
    ko_exp,
    nat_var_exp,
    how='outer',
    on='Gene',
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
)

all_exp = pd.merge(
    merged,
    stress_exp,
    how='outer',
    on='Gene',
    left_index=False,
    right_index=False,
    sort=True,
    copy=True,
)

# %%
import xgboost as xgb
import numpy as np
from sklearn.model_selection import train_test_split

y_target_gene = 'YAL001C'
X = 'everything else'

# DROP ALL NANS
all_exp = all_exp.dropna()

##### Prep Y value (target gene) #####
# select only the target gene row
y_pandas = all_exp.loc[all_exp['Gene'] == y_target_gene]
# drop the name of the target gene
y_pandas = y_pandas.drop(columns='Gene')
# convert expression values to np array (and un-nest it w/ flatten)
y_arr = y_pandas.to_numpy().flatten()


##### Prep X values (all other genes) #####

# select all EXCEPT target
X_pandas = all_exp.loc[all_exp['Gene'] != y_target_gene]
# drop the name of the target gene
X_pandas = X_pandas.drop(columns='Gene')
# convert expression values to np array (and un-nest it w/ flatten)
X_arr = X_pandas.to_numpy()

X_arr

# Run XGBoost

from sklearn.metrics import mean_squared_error as MSE
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_arr.transpose(), y_arr, test_size=0.2, random_state=42)

# %%
# start XGBOOST

# create the Xgboost specific DMatrix data format
dtrain = xgb.DMatrix(X_train, label=y_train, missing=np.NaN)
dtest = xgb.DMatrix(X_test, label=y_test,    missing=np.NaN)

# %%
# Grid search params

# Other params
# brute force scan for all parameters, here are the tricks
# usually max_depth is 6,7,8
# learning rate is around 0.05, but small changes may make big diff
# tuning min_child_weight subsample colsample_bytree can have much fun of fighting against overfit
# n_estimators is how many round of boosting
# finally, ensemble xgboost with multiple seeds may reduce variance

interesting_params = {'nthread':[4], # when use hyperthread, xgboost may become slower
              'objective':['binary:logistic'],
              'learning_rate': [0.05],
              'max_depth': [6],
              'min_child_weight': [11],
              'silent': [1],
              'subsample': [0.8],
              'colsample_bytree': [0.7],
              'n_estimators': [5], #number of trees, change it to 1000 for better results
              'missing':[-999],
              'seed': [1337]}

# %%
# train
xgb_r = xgb.XGBRegressor(objective ='reg:squarederror',
                  n_estimators = 100, seed = 42)
# bst = xgb.train(param, dtrain, num_round)

# %%
# Fitting the model
xgb_r.fit(X_train, y_train)


# %%
xgb_r = xgb.XGBRegressor(random_state=42)
params = {
    # Parameters that we are going to tune.
    'max_depth': range (3, 10),
    'n_estimators': range(10, 50),
    'learning_rate': [0.01, 0.2],
    # 'min_child_weight': 1,
    # 'eta':.3,
    # 'subsample': 1,
    # 'colsample_bytree': 1,
    # Other parameters
    'objective':['reg:squarederror', 'reg:tweedie']
}

# booster ['gbtree', 'gblinear', 'dart']

# 2 mins to run
params = {
    # Parameters that we are going to tune.
    'learning_rate': [0.2],
    # 'min_child_weight': 1,
    # 'eta':.3,
    # 'subsample': 1,
    # 'colsample_bytree': 1,
    # Other parameters
    'objective':['reg:squarederror']
}

# Various hyper-parameters to tune
kaggle_params = {
              'objective':['reg:squarederror'],
              'learning_rate': [0.001, .01, 0.05, .1, 0.2, 0.3], #so called `eta` value
              'max_depth': [3, 6, 10, 15, 20, 40],
              'min_child_weight': [0, .5, 1, 3, 7, 10],
            #   'silent': [1],
            #   'subsample': [0.7],
            #   'colsample_bytree': [0.7],
              'n_estimators': [20, 80, 120, 200, 500, 1000]
}

# %%

from sklearn.model_selection import GridSearchCV

# kas_params = {
#     'learning_rate': [0.2],
#     'objective':[ 'reg:squarederror'],
# }

# this took 30 minutes on M1.
xgb_grid = GridSearchCV(xgb_r,
                        kaggle_params,
                        # cv = 2,
                        n_jobs = 10, # -1 uses all cores
                        scoring = 'r2',
                        verbose=True)

# TODO: Figure out how to evaluate these models

# %%
# fit the model
xgb_grid.fit(X_train, y_train)

# %%
print(f"best params: {xgb_grid.best_params_}")

# %%
print(f"best score: {xgb_grid.best_score_}")

exit()


# %%
# trust your CV!
best_parameters, score, _ = max(xgb_grid.best_score_, key=lambda x: x[1])
print('Raw AUC score:', score)
for param_name in sorted(best_parameters.keys()):
    print("%s: %r" % (param_name, best_parameters[param_name]))

# %%
# optionally, save model
# xgb_r.save_model('testXBG_R.model')

# load saved model
xgb_r = xgb.Booster({'nthread': 4})  # init model
xgb_r.load_model('testXBG_R.model')  # load data

# %%
# Predict the model
pred = xgb_r.predict(X_test)

# RMSE Computation
rmse = np.sqrt(MSE(y_test, pred))
print("RMSE : % f" %(rmse))

# %%
# results
# 1000 trees = .582
# 100 trees = .594
# squarederror, 100 trees = 0.601592

# %%
xgb.plot_importance(xgb_r)


# %%
# viz tree(s)
xgb.to_graphviz(xgb_r, num_trees=2)

# worse version of viz with matplotlib
# xgb.plot_tree(xgb_r, num_trees=4) # change num_trees as desired

# %% [markdown]
# # Random testing below here

# %%


# %%
# random testing
from sklearn import datasets

iris = datasets.load_iris()
X = iris.data
y = iris.target

X.shape

# %%
y.shape

# %%
y

# %%


# %% [markdown]
# # Comparing our network to gold standard

# %%
## remounting here so I don't have to search above  for the box to run
from google.colab import drive
drive.mount('/content/drive')

# %%
import pandas as pd

# Gold standard networks are represented as TSVs, source \t sink
# Function to read a gold standard network file into an adjacency matrix
# Returns adjacency matrix
def read_net(filename):
  # read the adjacency list into a dataframe
  adj_list = pd.read_csv(filename, sep="\t", header=0, names=['source', 'sink'])

  # convert the adj list df to adj matrix df
  adj_mat = pd.crosstab(adj_list.source, adj_list.sink)

  # get the indexes to all the gene IDs in source and sink to make matrix square
  idx = adj_mat.columns.union(adj_mat.index)
  adj_mat = adj_mat.reindex(index=idx, columns=idx, fill_value=0)

  return adj_mat

mi2ko = read_net("/content/drive/MyDrive/Colab Notebooks/mlBioinfor/merlin-p_inferred_networks/yeast_networks/gold/MacIsaac2.KO.txt")
print(mi2ko)

# %%
# Function to compare two networks (adjacency lists as dicts)
# Returns tuple of three values: number edges unique to net 1, shared edges, number of edges unique to net 2
def compare_nets(net_1, net_2):
  uniq_1 = 0
  shared = 0
  uniq_2 = 0



