import time
import xgboost as xgb
import numpy as np
import pandas as pd
import dask.array as da
import dask.dataframe as dd
# import dask.distributed
from dask.distributed import Client
from dask_cuda import LocalCUDACluster

from sklearn.model_selection import GridSearchCV

'''
GLOBAL: Load data
'''
print("Starting file")

print("Using genes: all_exp_imputed_nans.csv")
gold_df = pd.read_csv('/home/kastanday/mlbioinforfinal/all_exp_imputed_nans.csv', index_col=0, header=0) # KEEP HEADER
ordered_gene_names_list = gold_df.get("Gene").tolist()
tf_name_list = pd.read_csv('TF_list.txt', header=None)
tf_name_list = tf_name_list.iloc[:, 0].to_list()



def get_pandas_data(y_target_gene):
  """
  y_target_gene = string of name
  return: X (PANDAS), y_arr (np array)
  """
  y_train = gold_df.loc[gold_df['Gene'] == y_target_gene]
  y_train = y_train.drop(columns='Gene')
  y_train = y_train.to_numpy().flatten()

  X_train = gold_df.loc[gold_df['Gene'] != y_target_gene]
  X_train = X_train[X_train['Gene'].isin(tf_name_list)]
  X_train = X_train.drop(columns='Gene')
  X_train = X_train.transpose()

  return X_train, y_train


def main():
  fullTrain = time.time()

  # MAIN LOOP
  target_gene_name = 'YGR088W'
  print("Target gene: ", target_gene_name)
  X_train, y_train = get_pandas_data(target_gene_name)

  print("X_train shape: ", X_train.shape)
  print("y_train shape: ", y_train.shape)

  # X_train = dd.from_pandas(X_train, npartitions=1)
  # y_train = da.from_array(y_train) # chunks = 'auto
  # dtrain = xgb.dask.DaskDMatrix(client=client, data=X_train, label=y_train)

  kaggle_params = {
            'objective':['reg:squarederror'],
            'learning_rate': [0.02, 0.05, 0.1],
            'max_depth': [4,6,8],
            'min_child_weight': [3, 5, 8],
            'n_estimators': [2000]
  }

  xgb_r = xgb.XGBRegressor(random_state=42)
  xgb_grid = GridSearchCV(xgb_r,
                          kaggle_params,
                          cv = 4,
                          n_jobs = -1, # -1 uses all cores
                          scoring = 'explained_variance',
                          verbose=3)

  # fit the model
  print("Fitting model...")
  xgb_grid.fit(X_train, y_train)

  print("SUCESSFULLY COPMPLETED")
  print(f"best params: {xgb_grid.best_params_}")
  print(f"best score: {xgb_grid.best_score_}")

  # nthread = number of threads per dask worker.
  # nthread 8 = 1.29 sec per run.
  # ntrhead 1 = 0.98 or 0.93 with 20 boosts, 1.25 with 40 boosts.
  # ntrhead 2 = 1.25
  # todo: delete n_estimators and verbose
  # 'n_estimators': 1000,

  gpu_time = time.time() - fullTrain
  print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
  print("Fin")



if __name__ == "__main__":
  ## memeory_limit = '64GB'
  # with LocalCUDACluster(n_workers=3, memory_limit="48 GiB") as cluster:
    # with Client(cluster) as client:
      # main(client)
  main()

