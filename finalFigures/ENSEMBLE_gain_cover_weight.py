import time
import xgboost as xgb
import numpy as np
import pandas as pd
import dask.array as da
import dask.dataframe as dd
# import dask.distributed
from dask.distributed import Client
from dask_cuda import LocalCUDACluster

'''
GLOBAL: Load data
'''
print("Starting file")

print("Using genes: all_exp_imputed_nans.csv")
gold_df = pd.read_csv('/home/kastanday/mlbioinforfinal/all_exp_imputed_nans.csv', index_col=0, header=0) # KEEP HEADER
ordered_gene_names_list = gold_df.get("Gene").tolist()

# print("Outfiles in: ./gain_cover_weight")
# gold_df = pd.read_csv('/home/kastanday/mlbioinforfinal/11th_work/gold_data/all_exp_imputed_nans_gold_standard.csv') # KEEP HEADER

print("Using genes: all_exp_imputed_nans.csv")
# gold_df = pd.read_csv('/home/kastanday/mlbioinforfinal/all_exp_imputed_nans.csv', index_col=0, header=0) # KEEP HEADER
# ordered_gene_names_list = gold_df.get("Gene").tolist()
tf_name_list = pd.read_csv('TF_list.txt', header=None)
tf_name_list = tf_name_list.iloc[:, 0].to_list()



def get_pandas_data(y_target_gene):
  """
  y_target_gene = string of name
  return: X (PANDAS), y_arr (np array)
  """
  y_train = gold_df.loc[gold_df['Gene'] == y_target_gene]
  y_train = y_train.drop(columns='Gene')
  y_train = y_train.to_numpy().flatten()

  X_train = gold_df.loc[gold_df['Gene'] != y_target_gene]
  X_train = X_train[X_train['Gene'].isin(tf_name_list)]
  X_train = X_train.drop(columns='Gene')
  X_train = X_train.transpose()

  return X_train, y_train


def main(client):
  feat_matrix_gain__1 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_gain__2 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_gain__3 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_cover_1 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_cover_2 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_cover_3 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_weight1 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_weight2 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_matrix_weight3 = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))

  all_matricies = [
    feat_matrix_gain__1,
    feat_matrix_gain__2,
    feat_matrix_gain__3,
    feat_matrix_cover_1,
    feat_matrix_cover_2,
    feat_matrix_cover_3,
    feat_matrix_weight1,
    feat_matrix_weight2,
    feat_matrix_weight3
    ]

  # ENSEMBLE OUTPUT:
  ensemble_ft_importance = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))

  fullTrain = time.time()

  ''' ADJUST START 👇👇👇'''
  first_half = True
  ''' ADJUST THIS  👆👆👆'''

  starting_idx = None
  stop_idx = None
  count = 0
  filename_prefix = ''

  if first_half:
    starting_idx = 0
    stop_idx = 99999
    filename_prefix = f"from{starting_idx}to{stop_idx}"
  else:
    starting_idx = 250
    stop_idx = 500
    filename_prefix = f"from{starting_idx}to{stop_idx}"


  # MAIN LOOP
  for idx, target_y_gene in enumerate(ordered_gene_names_list):

    if idx < starting_idx:
      continue
    oneRound = time.time()

    X_train, y_train = get_pandas_data(target_y_gene)

    X_train = dd.from_pandas(X_train, npartitions=1)
    y_train = da.from_array(y_train) # chunks = 'auto

    dtrain = xgb.dask.DaskDMatrix(client=client, data=X_train, label=y_train)

    # nthread = number of threads per dask worker.
    # nthread 8 = 1.29 sec per run.
    # ntrhead 1 = 0.98 or 0.93 with 20 boosts, 1.25 with 40 boosts.
    # ntrhead 2 = 1.25
    # todo: delete n_estimators and verbose
    # 'n_estimators': 1000,
    params = {'n_jobs': -1, 'nthread': 1, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 8, 'objective': 'reg:squarederror', 'seed': 42}
    print(params)

    output_model1 = xgb.dask.train(
    client,
    params,
    dtrain,
    num_boost_round=800,
    )

    params['seed'] = 420
    params['min_child_weight'] = 7

    output_model2 = xgb.dask.train(
    client,
    params,
    dtrain,
    num_boost_round=500,
    )

    params['seed'] = 69420
    params['min_child_weight'] =  5

    output_model3 = xgb.dask.train(
    client,
    params,
    dtrain,
    num_boost_round=500,
    )

    # average features importance across ensemble of models


    model1 = output_model1['booster']
    model2 = output_model2['booster']
    model3 = output_model3['booster']

    gain1 = model1.get_score(importance_type='total_gain')
    gain2 = model2.get_score(importance_type='total_gain')
    gain3 = model3.get_score(importance_type='total_gain')
    gain1 = {int(k.strip()):v for k,v in gain1.items()} # keys become ints
    gain2 = {int(k.strip()):v for k,v in gain2.items()} # keys become ints
    gain3 = {int(k.strip()):v for k,v in gain3.items()} # keys become ints

    cover1 = model1.get_score(importance_type='total_cover')
    cover2 = model2.get_score(importance_type='total_cover')
    cover3 = model3.get_score(importance_type='total_cover')
    cover1 = {int(k.strip()):v for k,v in cover1.items()} # keys become ints
    cover2 = {int(k.strip()):v for k,v in cover2.items()} # keys become ints
    cover3 = {int(k.strip()):v for k,v in cover3.items()} # keys become ints

    weight1 = model1.get_score(importance_type='weight')
    weight2 = model2.get_score(importance_type='weight')
    weight3 = model3.get_score(importance_type='weight')
    weight1 = {int(k.strip()):v for k,v in weight1.items()} # keys become ints
    weight2 = {int(k.strip()):v for k,v in weight2.items()} # keys become ints
    weight3 = {int(k.strip()):v for k,v in weight3.items()} # keys become ints

    for featNum, importance in gain1.items():
      feat_matrix_gain__1[idx][featNum] = importance

    for featNum, importance in gain2.items():
      feat_matrix_gain__2[idx][featNum] = importance

    for featNum, importance in gain3.items():
      feat_matrix_gain__3[idx][featNum] = importance

    for featNum, importance in cover1.items():
      feat_matrix_cover_1[idx][featNum] = importance

    for featNum, importance in cover2.items():
      feat_matrix_cover_2[idx][featNum] = importance

    for featNum, importance in cover3.items():
      feat_matrix_cover_3[idx][featNum] = importance

    for featNum, importance in weight1.items():
      feat_matrix_weight1[idx][featNum] = importance

    for featNum, importance in weight2.items():
      feat_matrix_weight2[idx][featNum] = importance

    for featNum, importance in weight3.items():
      feat_matrix_weight3[idx][featNum] = importance

    ensemble_ft_importance = (feat_matrix_gain__1 + feat_matrix_gain__2 + feat_matrix_gain__3 +
                         feat_matrix_cover_1 + feat_matrix_cover_2 + feat_matrix_cover_3 +
                         feat_matrix_weight1 + feat_matrix_weight2 + feat_matrix_weight3) / 9

    oneTree = time.time() - oneRound
    print(f"{idx} of unknownlol. (Count is: {count} till {stop_idx}). Time (sec): {oneTree.__round__(2)}")

    if (idx != 0) and (idx % 500) == 0:
      # progress save
      print("---------------- Progress save here---------------------")
      filename = f"./ensemble_result/PROGRESS_{str(idx)}_dec_18_ALLGENES_ensemble.csv"
      np.savetxt(filename, ensemble_ft_importance, delimiter=",")



  # if count == stop_idx:
  filename = f"./ensemble_result/FULL_dec_14_ALLGENES_ensemble.csv"
  np.savetxt(filename, ensemble_ft_importance, delimiter=",")

  for num, ftMatrix in enumerate(all_matricies):
    filename = f"./ensemble_result/FULL_matrx_{num}_individual_matrix.csv"
    np.savetxt(filename, ftMatrix, delimiter=",")

  gpu_time = time.time() - fullTrain
  print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
  print("Fin")
  print(f"STOPPIG EARLY")
  exit()
    # count += 1

  # np.savetxt("progress_multi/FULL_dec_11_ONLYGOLD_totalgain.csv", feat_imp_matrix, delimiter=",")

  gpu_time = time.time() - fullTrain
  print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
  print("Fin")


if __name__ == "__main__":
  ## memeory_limit = '64GB'
  with LocalCUDACluster(n_workers=3, memory_limit="48 GiB") as cluster:
    with Client(cluster) as client:
      main(client)

