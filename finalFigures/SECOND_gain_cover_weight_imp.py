import time
import xgboost as xgb
import numpy as np
import pandas as pd
import dask.array as da
import dask.dataframe as dd
# import dask.distributed
from dask.distributed import Client
from dask_cuda import LocalCUDACluster

'''
GLOBAL: Load data
'''
print("Starting file")
# print("Outfiles in: ./gain_cover_weight")
# gold_df = pd.read_csv('/home/kastanday/mlbioinforfinal/11th_work/gold_data/all_exp_imputed_nans_gold_standard.csv') # KEEP HEADER
# ordered_gene_names_list = pd.read_csv('/home/kastanday/mlbioinforfinal/11th_work/gold_data/ordered_gene_names_list_gold_standard.csv', header=None) # NO HEADER
# ordered_gene_names_list = ordered_gene_names_list.iloc[:, 0].to_list()

print("Using genes: all_exp_imputed_nans.csv")
gold_df = pd.read_csv('/home/kastanday/mlbioinforfinal/all_exp_imputed_nans.csv', index_col=0, header=0) # KEEP HEADER
ordered_gene_names_list = gold_df.get("Gene").tolist()


def get_pandas_data(y_target_gene):
  """
  y_target_gene = string of name
  return: X (PANDAS), y_arr (np array)
  """
  y_train = gold_df.loc[gold_df['Gene'] == y_target_gene]
  y_train = y_train.drop(columns='Gene')
  y_train = y_train.to_numpy().flatten()

  X_train = gold_df.loc[gold_df['Gene'] != y_target_gene]
  X_train = X_train.drop(columns='Gene')
  X_train = X_train.transpose()

  return X_train, y_train



def main(client):
  feat_imp_matrix_gain = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_imp_matrix_cover = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  feat_imp_matrix_weight = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))
  fullTrain = time.time()

  ''' ADJUST START 👇👇👇'''
  first_half = False
  ''' ADJUST THIS  👆👆👆'''

  starting_idx = None
  stop_idx = None
  filename_prefix = ''

  if first_half:
    starting_idx = 0 
    stop_idx = 250
    filename_prefix = f"from{starting_idx}to{stop_idx}"
  else:
    starting_idx = 1001
    stop_idx = 2000
    filename_prefix = f"from{starting_idx}to{stop_idx}"
  

  max_runs = len(ordered_gene_names_list)

  # MAIN LOOP 
  for idx, target_y_gene in enumerate(ordered_gene_names_list):

    if idx < starting_idx:
      continue
    oneRound = time.time()

    X_train, y_train = get_pandas_data(target_y_gene)

    X_train = dd.from_pandas(X_train, npartitions=1)
    y_train = da.from_array(y_train) # chunks = 'auto

    dtrain = xgb.dask.DaskDMatrix(client=client, data=X_train, label=y_train)

    # nthread = number of threads per dask worker.
    # nthread 8 = 1.29 sec per run.
    # ntrhead 1 = 0.98 or 0.93 with 20 boosts, 1.25 with 40 boosts.
    # ntrhead 2 = 1.25
    # todo: delete n_estimators and verbose
    # 'n_estimators': 1000,
    params = {'n_jobs': -1, 'nthread': 1, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 7, 'objective': 'reg:squarederror', 'seed': 42}

    output = xgb.dask.train(
    client,
    params,
    dtrain,
    num_boost_round=500,
    )

    xgb_r = output['booster']

    ##### TOTAL GAIN
    featImportanceDict = xgb_r.get_score(importance_type='total_gain')
    featImportanceDict = {int(k.strip()):v for k,v in featImportanceDict.items()} # keys become ints

    for featNum, importance in featImportanceDict.items():
      feat_imp_matrix_gain[idx][featNum] = importance
    
    ###### COVER
    featImportanceDict_cover = xgb_r.get_score(importance_type='total_cover')
    featImportanceDict_cover = {int(k.strip()):v for k,v in featImportanceDict_cover.items()} # keys become ints

    for featNum, importance in featImportanceDict_cover.items():
      feat_imp_matrix_cover[idx][featNum] = importance

    ###### WEIGHT
    featImportanceDict_weight = xgb_r.get_score(importance_type='total_cover')
    featImportanceDict_weight = {int(k.strip()):v for k,v in featImportanceDict_weight.items()} # keys become ints

    for featNum, importance in featImportanceDict_weight.items():
      feat_imp_matrix_weight[idx][featNum] = importance

    oneTree = time.time() - oneRound
    print(f"{idx} of {stop_idx}. Time (sec): {oneTree.__round__(2)}")

    if idx >= stop_idx:
      # OUTSIDE TRAIN LOOP
      print("Saving to folder: real_genes")
      filename = f"real_genes/{str(idx)}{filename_prefix}_dec_13_ONLYGOLD_totalgain.csv"
      np.savetxt(filename, feat_imp_matrix_gain, delimiter=",")

      filename = f"real_genes/{str(idx)}{filename_prefix}_dec_13_ONLYGOLD_totalcover.csv"
      np.savetxt(filename, feat_imp_matrix_cover, delimiter=",")

      filename = f"real_genes/{str(idx)}{filename_prefix}_dec_13_ONLYGOLD_totalweight.csv"
      np.savetxt(filename, feat_imp_matrix_weight, delimiter=",")

      gpu_time = time.time() - fullTrain
      print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
      print("Fin")
      exit()



if __name__ == "__main__":
  ## memeory_limit = '64GB'
  with LocalCUDACluster(n_workers=4, memory_limit="48 GiB") as cluster:
    with Client(cluster) as client:
      main(client)

