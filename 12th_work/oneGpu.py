import time
import xgboost as xgb
import numpy as np
import pandas as pd

'''
GLOBAL: Load data
'''
print("Starting file")
gold_df = pd.read_csv('gold_data/all_exp_imputed_nans_gold_standard.csv') # KEEP HEADER
ordered_gene_names_list = pd.read_csv('gold_data/ordered_gene_names_list_gold_standard.csv', header=None) # NO HEADER
ordered_gene_names_list = ordered_gene_names_list.iloc[:, 0].to_list()

# pandas data fetch. No splitting.
def get_pandas_data(y_target_gene):
  """
  y_target_gene = string of name
  return: X (PANDAS), y_arr (np array)
  """
  y_train = gold_df.loc[gold_df['Gene'] == y_target_gene]
  y_train = y_train.drop(columns='Gene')
  y_train = y_train.to_numpy().flatten()

  X_train = gold_df.loc[gold_df['Gene'] != y_target_gene]
  X_train = X_train.drop(columns='Gene')
  X_train = X_train.transpose()

  return X_train, y_train

def main():
  feat_imp_matrix = np.zeros(shape=(len(ordered_gene_names_list), len(ordered_gene_names_list)))

  print("STARTING TRAINING LOOP")

  fullTrain = time.time()
  stop_early = 0
  for idx, target_y_gene in enumerate(ordered_gene_names_list):
    oneRound = time.time()
    X_train, y_train = get_pandas_data(target_y_gene)
    # x.shape = 6270 x 954
    # y.shape = 954

    # might need 'gpu_id': 0 
    params = {'n_jobs': -1, 'nthread': 1, 'tree_method': 'gpu_hist', 'learning_rate': 0.1, 'max_depth': 2, 'min_child_weight': 8, 'n_estimators': 1000, 'objective': 'reg:squarederror', 'seed': 42}

    xgb_r = xgb.XGBRegressor(**params)
    # xgb_r.fit(X_train, y_train)
    xgb_r.fit(X_train, y_train)

    featImportanceDict = xgb_r.get_booster().get_score(importance_type='gain') # total_gain, weight 

    # also strip white space. Convert to int.
    # also... I didn't see any f in there 
    featImportanceDict = {int(k.strip()):v for k,v in featImportanceDict.items()} # keys become ints

    for featNum, importance in featImportanceDict.items():
      # print(f'idx: {idx}, featNum {featNum}')
      feat_imp_matrix[idx][featNum] = importance
    
    if stop_early != 0 and stop_early % 500 == 0:
      filename = f"{str(idx)}_PROGRESS_dec_11_FULL_ONLYGOLD_importance"
      np.savetxt(filename, feat_imp_matrix, delimiter=",")
    stop_early += 1

    oneTree = time.time() - oneRound
    print(f"{idx} of 3,100. Time (sec): {oneTree.__round__(2)}")

  np.savetxt("_dec_13_FULL_ONLYGOLD_importance.csv", feat_imp_matrix, delimiter=",")

  gpu_time = time.time() - fullTrain
  print(f"GPU Training Time (sec): {gpu_time.__round__(2)}")
  print("Fin")


if __name__ == "__main__":
  main()


